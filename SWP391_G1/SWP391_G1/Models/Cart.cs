﻿using System;
using System.Collections.Generic;

namespace SWP391_G1.Models
{
    public partial class Cart
    {
        public int RecordId { get; set; }
        public int? UserId { get; set; }
        public int? ProductId { get; set; }
        public int? Count { get; set; }
        public DateTime? DateCreated { get; set; }

        public virtual Product? Product { get; set; }
        public virtual User? User { get; set; }
    }
}
