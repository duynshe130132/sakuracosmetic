﻿using System;
using System.Collections.Generic;

namespace SWP391_G1.Models
{
    public partial class ReservationCart
    {
        public int RecordId { get; set; }
        public int? UserId { get; set; }
        public int? ServiceId { get; set; }
        public int? StaffId { get; set; }
        public DateTime ReservationTime { get; set; }
        public DateTime? DateCreated { get; set; }

        public virtual Service? Service { get; set; }
        public virtual Employee? Staff { get; set; }
        public virtual User? User { get; set; }
    }
}
