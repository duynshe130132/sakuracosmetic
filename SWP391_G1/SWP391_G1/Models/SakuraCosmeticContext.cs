﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SWP391_G1.Models
{
    public partial class SakuraCosmeticContext : DbContext
    {
        public SakuraCosmeticContext()
        {
        }

        public SakuraCosmeticContext(DbContextOptions<SakuraCosmeticContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Blog> Blogs { get; set; } = null!;
        public virtual DbSet<BlogCategory> BlogCategories { get; set; } = null!;
        public virtual DbSet<Cart> Carts { get; set; } = null!;
        public virtual DbSet<Category> Categories { get; set; } = null!;
        public virtual DbSet<Comment> Comments { get; set; } = null!;
        public virtual DbSet<Employee> Employees { get; set; } = null!;
        public virtual DbSet<Order> Orders { get; set; } = null!;
        public virtual DbSet<OrderDetail> OrderDetails { get; set; } = null!;
        public virtual DbSet<Product> Products { get; set; } = null!;
        public virtual DbSet<Reservation> Reservations { get; set; } = null!;
        public virtual DbSet<ReservationCart> ReservationCarts { get; set; } = null!;
        public virtual DbSet<ReservationDetail> ReservationDetails { get; set; } = null!;
        public virtual DbSet<Role> Roles { get; set; } = null!;
        public virtual DbSet<Service> Services { get; set; } = null!;
        public virtual DbSet<ServiceCategory> ServiceCategories { get; set; } = null!;
        public virtual DbSet<User> Users { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
                optionsBuilder.UseSqlServer(config.GetConnectionString("connection"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Blog>(entity =>
            {
                entity.Property(e => e.BlogId).HasColumnName("Blog_Id");

                entity.Property(e => e.BlogTitle).HasColumnName("Blog_title");

                entity.Property(e => e.DateModified).HasColumnType("datetime");

                entity.HasOne(d => d.Cate)
                    .WithMany(p => p.Blogs)
                    .HasForeignKey(d => d.CateId)
                    .HasConstraintName("FK_Blogs_Blog_Categories");
            });

            modelBuilder.Entity<BlogCategory>(entity =>
            {
                entity.ToTable("Blog_Categories");
            });

            modelBuilder.Entity<Cart>(entity =>
            {
                entity.HasKey(e => e.RecordId)
                    .HasName("PK__Carts__603930688B159E9D");

                entity.Property(e => e.RecordId).HasColumnName("Record_id");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.ProductId).HasColumnName("Product_id");

                entity.Property(e => e.UserId).HasColumnName("User_id");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Carts)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK__Carts__Product_id__32E0915F");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Carts)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_Carts_User");
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.ToTable("Category");

                entity.Property(e => e.CategoryId).HasColumnName("Category_id");

                entity.Property(e => e.CategoryDescription).HasColumnName("Category_description");

                entity.Property(e => e.CategoryName).HasColumnName("Category_name");
            });

            modelBuilder.Entity<Comment>(entity =>
            {
                entity.ToTable("Comment");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Comment1)
                    .IsUnicode(false)
                    .HasColumnName("Comment");

                entity.HasOne(d => d.Blog)
                    .WithMany(p => p.Comments)
                    .HasForeignKey(d => d.BlogId)
                    .HasConstraintName("FK__Comment__BlogId__4F47C5E3");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Comments)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK__Comment__UserId__4E53A1AA");
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_Employees_Role");
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.ToTable("Order");

                entity.Property(e => e.OrderId).HasColumnName("Order_id");

                entity.Property(e => e.OrderDate)
                    .HasColumnType("datetime")
                    .HasColumnName("Order_Date");

                entity.Property(e => e.Total).HasColumnType("numeric(10, 2)");

                entity.Property(e => e.UserId).HasColumnName("User_id");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_Order_User");
            });

            modelBuilder.Entity<OrderDetail>(entity =>
            {
                entity.Property(e => e.OrderDetailId).HasColumnName("OrderDetail_id");

                entity.Property(e => e.OrderId).HasColumnName("Order_id");

                entity.Property(e => e.ProductId).HasColumnName("Product_id");

                entity.Property(e => e.UnitPrice).HasColumnType("numeric(10, 2)");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.OrderDetails)
                    .HasForeignKey(d => d.OrderId)
                    .HasConstraintName("FK__OrderDeta__Order__300424B4");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.OrderDetails)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK__OrderDeta__Product___30F848ED");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.Property(e => e.ProductId).HasColumnName("Product_id");

                entity.Property(e => e.CategoryId).HasColumnName("Category_id");

                entity.Property(e => e.ProductDescription).HasColumnName("Product_description");

                entity.Property(e => e.ProductImage).HasColumnName("Product_image");

                entity.Property(e => e.ProductName).HasColumnName("Product_name");

                entity.Property(e => e.ProductPrice)
                    .HasColumnType("numeric(10, 2)")
                    .HasColumnName("Product_price");

                entity.Property(e => e.ProductStatus).HasColumnName("Product_status");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK__Products__Category___31EC6D26");
            });

            modelBuilder.Entity<Reservation>(entity =>
            {
                entity.ToTable("Reservation");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Reservations)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_Reservation_User");
            });

            modelBuilder.Entity<ReservationCart>(entity =>
            {
                entity.HasKey(e => e.RecordId);

                entity.ToTable("Reservation_Carts");

                entity.Property(e => e.RecordId).HasColumnName("Record_id");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.ReservationTime).HasColumnType("datetime");

                entity.Property(e => e.ServiceId).HasColumnName("Service_id");

                entity.Property(e => e.StaffId).HasColumnName("Staff_Id");

                entity.Property(e => e.UserId).HasColumnName("User_id");

                entity.HasOne(d => d.Service)
                    .WithMany(p => p.ReservationCarts)
                    .HasForeignKey(d => d.ServiceId)
                    .HasConstraintName("FK_Reservation_Carts_Services");

                entity.HasOne(d => d.Staff)
                    .WithMany(p => p.ReservationCarts)
                    .HasForeignKey(d => d.StaffId)
                    .HasConstraintName("FK_Reservation_Carts_Employees");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.ReservationCarts)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_Reservation_Carts_User");
            });

            modelBuilder.Entity<ReservationDetail>(entity =>
            {
                entity.Property(e => e.ReserTime).HasColumnType("datetime");

                entity.Property(e => e.ServicePrice).HasColumnType("numeric(10, 0)");

                entity.HasOne(d => d.Reservation)
                    .WithMany(p => p.ReservationDetails)
                    .HasForeignKey(d => d.ReservationId)
                    .HasConstraintName("FK_ReservationDetails_Reservation");

                entity.HasOne(d => d.Service)
                    .WithMany(p => p.ReservationDetails)
                    .HasForeignKey(d => d.ServiceId)
                    .HasConstraintName("FK_ReservationDetails_Services");

                entity.HasOne(d => d.Staff)
                    .WithMany(p => p.ReservationDetails)
                    .HasForeignKey(d => d.StaffId)
                    .HasConstraintName("FK_ReservationDetails_Employees");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("Role");

                entity.Property(e => e.RoleId).HasColumnName("Role_id");

                entity.Property(e => e.RoleName)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("Role_name");
            });

            modelBuilder.Entity<Service>(entity =>
            {
                entity.Property(e => e.Price).HasColumnType("numeric(10, 2)");

                entity.HasOne(d => d.Cate)
                    .WithMany(p => p.Services)
                    .HasForeignKey(d => d.CateId)
                    .HasConstraintName("FK_Services_ServiceCategories");
            });

            modelBuilder.Entity<ServiceCategory>(entity =>
            {
                entity.Property(e => e.CateName).HasMaxLength(50);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("User");

                entity.Property(e => e.UserId).HasColumnName("User_id");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
