﻿using System;
using System.Collections.Generic;

namespace SWP391_G1.Models
{
    public partial class ServiceCategory
    {
        public ServiceCategory()
        {
            Services = new HashSet<Service>();
        }

        public int Id { get; set; }
        public string? CateName { get; set; }
        public bool? Status { get; set; }

        public virtual ICollection<Service> Services { get; set; }
    }
}
