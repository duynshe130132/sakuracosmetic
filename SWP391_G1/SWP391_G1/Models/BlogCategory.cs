﻿using System;
using System.Collections.Generic;

namespace SWP391_G1.Models
{
    public partial class BlogCategory
    {
        public BlogCategory()
        {
            Blogs = new HashSet<Blog>();
        }

        public int Id { get; set; }
        public string? CateName { get; set; }

        public virtual ICollection<Blog> Blogs { get; set; }
    }
}
