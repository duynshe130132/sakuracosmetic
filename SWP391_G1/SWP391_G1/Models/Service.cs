﻿using System;
using System.Collections.Generic;

namespace SWP391_G1.Models
{
    public partial class Service
    {
        public Service()
        {
            ReservationCarts = new HashSet<ReservationCart>();
            ReservationDetails = new HashSet<ReservationDetail>();
        }

        public int ServiceId { get; set; }
        public string? ServiceName { get; set; }
        public decimal Price { get; set; }
        public string? Description { get; set; }
        public int? Subcriber { get; set; }
        public int? CateId { get; set; }
        public bool? Status { get; set; }
        public string? ServiceImage { get; set; }

        public virtual ServiceCategory? Cate { get; set; }
        public virtual ICollection<ReservationCart> ReservationCarts { get; set; }
        public virtual ICollection<ReservationDetail> ReservationDetails { get; set; }
    }
}
