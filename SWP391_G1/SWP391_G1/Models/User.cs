﻿using System;
using System.Collections.Generic;

namespace SWP391_G1.Models
{
    public partial class User
    {
        public User()
        {
            Carts = new HashSet<Cart>();
            Comments = new HashSet<Comment>();
            Orders = new HashSet<Order>();
            ReservationCarts = new HashSet<ReservationCart>();
            Reservations = new HashSet<Reservation>();
        }

        public int UserId { get; set; }
        public string? UserName { get; set; }
        public string? Password { get; set; }
        public string? FullName { get; set; }
        public string? Address { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public string? Country { get; set; }
        public string? Phone { get; set; }
        public string? Email { get; set; }
        public bool? Status { get; set; }
        public string? AvatarUrl { get; set; }

        public virtual ICollection<Cart> Carts { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<ReservationCart> ReservationCarts { get; set; }
        public virtual ICollection<Reservation> Reservations { get; set; }
    }
}
