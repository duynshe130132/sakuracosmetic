﻿using System;
using System.Collections.Generic;

namespace SWP391_G1.Models
{
    public partial class Reservation
    {
        public Reservation()
        {
            ReservationDetails = new HashSet<ReservationDetail>();
        }

        public int ReservationId { get; set; }
        public int? UserId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool? Status { get; set; }

        public virtual User? User { get; set; }
        public virtual ICollection<ReservationDetail> ReservationDetails { get; set; }
    }
}
