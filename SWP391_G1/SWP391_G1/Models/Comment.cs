﻿using System;
using System.Collections.Generic;

namespace SWP391_G1.Models
{
    public partial class Comment
    {
        public int Id { get; set; }
        public string? Comment1 { get; set; }
        public int? UserId { get; set; }
        public int? BlogId { get; set; }

        public virtual Blog? Blog { get; set; }
        public virtual User? User { get; set; }
    }
}
