﻿using System;
using System.Collections.Generic;

namespace SWP391_G1.Models
{
    public partial class Blog
    {
        public Blog()
        {
            Comments = new HashSet<Comment>();
        }

        public int BlogId { get; set; }
        public string? BlogTitle { get; set; }
        public DateTime? DateModified { get; set; }
        public string? Description { get; set; }
        public int? CateId { get; set; }
        public string? BlogImageUrl { get; set; }
        public bool? FamousStatus { get; set; }

        public virtual BlogCategory? Cate { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
