﻿using System;
using System.Collections.Generic;

namespace SWP391_G1.Models
{
    public partial class Employee
    {
        public Employee()
        {
            ReservationCarts = new HashSet<ReservationCart>();
            ReservationDetails = new HashSet<ReservationDetail>();
        }

        public int Id { get; set; }
        public string? UserName { get; set; }
        public string? Password { get; set; }
        public string? FullName { get; set; }
        public string? Address { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public string? Country { get; set; }
        public string? Phone { get; set; }
        public string? Email { get; set; }
        public int? RoleId { get; set; }
        public bool? Status { get; set; }
        public string? AvatarUrl { get; set; }

        public virtual Role? Role { get; set; }
        public virtual ICollection<ReservationCart> ReservationCarts { get; set; }
        public virtual ICollection<ReservationDetail> ReservationDetails { get; set; }
    }
}
