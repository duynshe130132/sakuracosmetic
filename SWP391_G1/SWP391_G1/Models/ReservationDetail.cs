﻿using System;
using System.Collections.Generic;

namespace SWP391_G1.Models
{
    public partial class ReservationDetail
    {
        public int Id { get; set; }
        public int? ServiceId { get; set; }
        public int? StaffId { get; set; }
        public decimal? ServicePrice { get; set; }
        public DateTime? ReserTime { get; set; }
        public int? ReservationId { get; set; }

        public virtual Reservation? Reservation { get; set; }
        public virtual Service? Service { get; set; }
        public virtual Employee? Staff { get; set; }
    }
}
