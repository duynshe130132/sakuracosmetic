﻿using System;
using System.Collections.Generic;

namespace SWP391_G1.Models
{
    public partial class Order
    {
        public Order()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }

        public int OrderId { get; set; }
        public DateTime? OrderDate { get; set; }
        public string? UserName { get; set; }
        public decimal? Total { get; set; }
        public int? UserId { get; set; }
        public bool? Status { get; set; }

        public virtual User? User { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
