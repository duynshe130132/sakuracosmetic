﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
namespace SWP391_G1.Middleware
{
    public class ViewDataMiddleware
    {
        private readonly RequestDelegate _next;
        public ViewDataMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
                httpContext.Items["id"] = httpContext.Session.GetInt32("id");
                httpContext.Items["password"] = httpContext.Session.GetString("password");
                httpContext.Items["username"] = httpContext.Session.GetString("username");
                httpContext.Items["avatar"] = httpContext.Session.GetString("avatar");
                httpContext.Items["ReservationCarts"] = httpContext.Session.GetString("ReservationCarts");
            httpContext.Items["ProductCarts"] = httpContext.Session.GetString("ProductCarts");
            await _next(httpContext);
        }
        
    }
    public static class ViewDataMiddlewareExtensions
    {
        public static IApplicationBuilder UseViewDataMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ViewDataMiddleware>();
        }
    }
}
