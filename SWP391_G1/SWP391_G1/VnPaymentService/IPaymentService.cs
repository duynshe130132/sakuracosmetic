﻿using SWP391_G1.Models;

namespace SWP391_G1.VnPaymentService
{
    public interface IPaymentService
    {
        string CreatePaymentUrl(PaymentInformationModel model, HttpContext context);
        PaymentResponseModel PaymentExecute(IQueryCollection collections);
    }
}
