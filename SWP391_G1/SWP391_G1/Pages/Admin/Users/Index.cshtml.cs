using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SWP391_G1.Models;
using SWP391_G1.Services;

namespace SWP391_G1.Pages.Admin.Users
{
    public class IndexModel : PageModel
    {
        public static SakuraCosmeticContext context = new SakuraCosmeticContext();
        //PaginatedList<Employee> employees = new PaginatedList<Employee>(context.Employees.Include(x => x.Role).ToList(), context.Employees.Count(), 1, 10);
        [BindProperty(SupportsGet = true)]
        public string? SearchString { get; set; }

        [BindProperty(SupportsGet = true)]
        public int IndexPaging { get; set; } = 1;

        [BindProperty(Name = "id", SupportsGet = true)]
        public int Id { get; set; }

        public int TotalPage { get; set; }
        public int UserCount { get; set; }
        [BindProperty(SupportsGet = true)]
        public IList<User> Users { get; set; } = default;
        [BindProperty(SupportsGet = true)]
        public int PageSize { get; set; } = default;
        public IActionResult OnGet(string searchString, int indexPaging, int pageSize)
        {
            if (HttpContext.Session.GetInt32("id") == null && HttpContext.Session.GetInt32("RoleId") != null && HttpContext.Session.GetInt32("RoleId") != 1)
            {
                return RedirectToPage("/Index");
            }
            PaginatedList<User> users;
            if (pageSize == 0)
            {
                pageSize = context.Users.ToList().Count;
            }
            var listUser = String.IsNullOrEmpty(searchString) ? context.Users.ToList() : context.Users.Where(x => x.FullName.Contains(searchString) || x.UserName.Contains(searchString)).ToList();
            users = new PaginatedList<User>(listUser, listUser.Count, 1, 4);

            TotalPage = users.TotalPages;
            UserCount = context.Users.ToList().Count;
            Users = PaginatedList<User>.Create(users.AsQueryable<User>(), indexPaging, 4);
            return Page();
        }
        public IActionResult OnPostDisableEmployee(int id)
        {
            var UserToUpdate = context.Users.FirstOrDefault(x => x.UserId == id);
            if (UserToUpdate != null)
            {
                UserToUpdate.Status = false;
                context.SaveChanges();
            }
            TempData["ToastMessage"] = "User disabled successfully.";
            TempData["ToastType"] = "success";
            return RedirectToPage("/Admin/Users/Index");
        }

        public IActionResult OnPostEnableEmployee(int id)
        {
            var UserToUpdate = context.Users.FirstOrDefault(x => x.UserId == id);
            if (UserToUpdate != null)
            {
                UserToUpdate.Status = true;
                context.SaveChanges();
            }
            TempData["ToastMessage"] = "User enabled successfully.";
            TempData["ToastType"] = "success";
            return RedirectToPage("/Admin/Users/Index");
        }
    }
}
