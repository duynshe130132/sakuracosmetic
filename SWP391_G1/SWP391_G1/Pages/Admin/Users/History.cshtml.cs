using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using SWP391_G1.Models;
using SWP391_G1.Services;

namespace SWP391_G1.Pages.Admin.Users
{
    public class HistoryModel : PageModel
    {
        public static SakuraCosmeticContext context = new SakuraCosmeticContext();
        //PaginatedList<Employee> employees = new PaginatedList<Employee>(context.Employees.Include(x => x.Role).ToList(), context.Employees.Count(), 1, 10);
       

        [BindProperty(SupportsGet = true)]
        public int IndexPaging { get; set; } = 1;

        [BindProperty(Name = "id", SupportsGet = true)]
        public int Id { get; set; }

        public int TotalPage { get; set; }
        public int OrderCount { get; set; }
        [BindProperty(SupportsGet = true)]
        public IList<Order> Orders { get; set; } = default;
        [BindProperty(SupportsGet = true)]
        public int PageSize { get; set; } = default;
        public IActionResult OnGet( int indexPaging, int pageSize, int? id)
        {
            if (HttpContext.Session.GetInt32("id") == null && HttpContext.Session.GetInt32("RoleId") != null && HttpContext.Session.GetInt32("RoleId") != 1)
            {
                return RedirectToPage("/Index");
            }
            PaginatedList<Order> orders;
            if (pageSize == 0)
            {
                pageSize = context.Orders.ToList().Count;
            }
            var listOrder = context.Orders.Include(o => o.User).Where(o=> o.UserId == id).ToList();
            orders = new PaginatedList<Order>(listOrder, listOrder.Count, 1, pageSize);

            TotalPage = orders.TotalPages;
            OrderCount = context.Orders.ToList().Count;
            Orders = PaginatedList<Order>.Create(orders.AsQueryable<Order>(), indexPaging, pageSize);
            return Page();
        }

    }
}
