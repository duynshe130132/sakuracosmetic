using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SWP391_G1.Models;
using SWP391_G1.Services;
using System.Linq;

namespace SWP391_G1.Pages.Admin.Products
{
    public class IndexModel : PageModel
    {
        public static SakuraCosmeticContext context;
        //PaginatedList<Service> services = new PaginatedList<Service>(context.Services.Include(x => x.Cate).ToList(), context.Services.Count(), 1, 10);
        public IndexModel(SakuraCosmeticContext _context)
        {
            context= _context;
        }
        [BindProperty(SupportsGet = true)]
        public string? SearchString { get; set; }

        public SelectList? Categories { get; set; }

        [BindProperty(SupportsGet = true)]
        public int CategoryId { get; set; } = 0;

        [BindProperty(SupportsGet = true)]
        public int IndexPaging { get; set; } = 1;

        [BindProperty(Name = "id", SupportsGet = true)]
        public int Id { get; set; }

        public int TotalPage { get; set; }
        [BindProperty(SupportsGet = true)]
        public Product Product { get; set; }
        public int ProductCount { get; set; }
        [BindProperty(SupportsGet = true)]
        public IList<Category> ProductCategory { get; set; } = default;
        [BindProperty(SupportsGet = true)]
        public IList<Product> Products { get; set; } = default;
        [BindProperty(SupportsGet = true)]
        public int PageSize { get; set; } = default;
        public IActionResult OnGet(int categoryId, string searchString, int indexPaging, int pageSize)
        {
            if (HttpContext.Session.GetInt32("id") == null)
            {
                return RedirectToPage("/Index");
            }
            PaginatedList<Product> products;
            if (pageSize == 0)
            {
                pageSize = context.Products.ToList().Count;
            }
            if (categoryId != 0)
            {
                var listProduct = String.IsNullOrEmpty(searchString) ? context.Products.Include(x => x.Category).Where(x => x.CategoryId == categoryId).ToList() : context.Products.Include(x => x.Category).Where(x => x.CategoryId == categoryId && x.ProductName.Contains(searchString)).ToList();
                products = new PaginatedList<Product>(listProduct, listProduct.Count, 1, 3);
            }
            else
            {
                var listProduct = String.IsNullOrEmpty(searchString) ? context.Products.Include(x => x.Category).ToList() : context.Products.Include(x => x.Category).Where(x => x.ProductName.Contains(searchString)).ToList();
                products = new PaginatedList<Product>(listProduct, listProduct.Count, 1, 3);
            }

            TotalPage = products.TotalPages;
            List<Category> productCategory = context.Categories.ToList();
            ProductCount = context.Products.ToList().Count;
            ProductCategory = productCategory;
            ViewData["categoryList"] = productCategory;
            ViewData["CategoryId"] = new SelectList(productCategory, "CategoryId", "CategoryName");
            Products = PaginatedList<Product>.Create(products.AsQueryable<Product>(), indexPaging, 3);

            return Page();
        }
        
        public IActionResult OnPostDisableProduct(int id)
        {
            var productToUpdate = context.Products.FirstOrDefault(x => x.ProductId == id);
            if (productToUpdate != null)
            {
                productToUpdate.ProductStatus = 0;
                context.SaveChanges();
            }
            TempData["ToastMessage"] = "Product disabled successfully.";
            TempData["ToastType"] = "success";
            return RedirectToPage("/Admin/Products/Index");
        }

        public IActionResult OnPostEnableProduct(int id)
        {
            var productToUpdate = context.Products.FirstOrDefault(x => x.ProductId == id);
            if (productToUpdate != null)
            {
                productToUpdate.ProductStatus = 1;
                context.SaveChanges();
            }
            TempData["ToastMessage"] = "Product enabled successfully.";
            TempData["ToastType"] = "success";
            return RedirectToPage("/Admin/Products/Index");
        }


    }
}
