using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using SWP391_G1.Models;
using SWP391_G1.Services;

namespace SWP391_G1.Pages.Admin.Products
{
    public class CreateModel : PageModel
    {
        private readonly SWP391_G1.Models.SakuraCosmeticContext _context;
        private readonly FlieUploadService service;
        public string filePath { get; set; } = default!;
        [BindProperty(SupportsGet =true)]
        public string ProductDescription { get; set; }
        public string ErrorValidImage { get; set; }
        public CreateModel(SWP391_G1.Models.SakuraCosmeticContext context, FlieUploadService _service)
        {
            _context = context;
            service = _service;
        }

        public IActionResult OnGet()
        {
            ViewData["CateId"] = new SelectList(_context.ServiceCategories, "Id", "CateName");
            return Page();
        }


        [BindProperty(SupportsGet = true)]
        public Product ProductCategory { get; set; } = default!;


        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync(IFormFile file)
        {           
            if(file.Length > (25 * 1024 * 1024) || !file.ContentType.StartsWith("image/"))
            {
                ErrorValidImage = "Image Is Invalid.";
                return Page();
            }
            if (file != null)
            {
                filePath = await service.DriveUploadBasic(file);
                ProductCategory.ProductDescription = ProductDescription;
                ProductCategory.ProductImage = filePath;
            }
            _context.Products.Add(ProductCategory);
            await _context.SaveChangesAsync();

            return RedirectToPage("/Admin/Products/Index");
        }
    }
}
