using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SWP391_G1.Models;
using SWP391_G1.Services;

namespace SWP391_G1.Pages.Admin.Products
{
    public class EditModel : PageModel
    {
        private readonly SWP391_G1.Models.SakuraCosmeticContext _context;
        private readonly FlieUploadService uploadservice = new FlieUploadService();
        [BindProperty(SupportsGet = true)]
        public string ProductDescription { get; set; }
        [BindProperty(SupportsGet = true)]
        public string PreImageurl { get; set; }
        public string ErrorValidImage { get; set; }
        public EditModel(SWP391_G1.Models.SakuraCosmeticContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Product Product { get; set; } = default!;
       
        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null || _context.Products == null)
            {
                return NotFound();
            }
            ViewData["CateId"] = new SelectList(_context.ServiceCategories, "Id", "CateName");
            var product = await _context.Products.FirstOrDefaultAsync(m => m.ProductId == id);
            PreImageurl = "https://drive.google.com/uc?id=" + product.ProductImage;
            if (product == null)
            {
                return NotFound();
            }
            Product = product;
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync(IFormFile file)
        {
            Product.ProductDescription = ProductDescription;
            if (file != null)
            {
                if (file.Length > (25 * 1024 * 1024) || !file.ContentType.StartsWith("image/"))
                {
                    ErrorValidImage = "Image Is Invalid.";
                    return Page();
                }
                bool isDelete = await uploadservice.DeleteFile(Product.ProductImage);
                var filePath = await uploadservice.DriveUploadBasic(file);
                Product.ProductImage = filePath;
            }
            _context.Attach(Product).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(Product.ProductId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool ProductExists(int id)
        {
            return (_context.Products?.Any(e => e.ProductId == id)).GetValueOrDefault();
        }
    }
}
