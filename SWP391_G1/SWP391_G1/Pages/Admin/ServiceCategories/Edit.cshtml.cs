﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SWP391_G1.Models;

namespace SWP391_G1.Pages.Admin.ServiceCategories
{
    public class EditModel : PageModel
    {
        private readonly SWP391_G1.Models.SakuraCosmeticContext _context;

        public EditModel(SWP391_G1.Models.SakuraCosmeticContext context)
        {
            _context = context;
        }

        [BindProperty]
        public ServiceCategory ServiceCategory { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (HttpContext.Session.GetInt32("id") == null && HttpContext.Session.GetInt32("RoleId") != null && HttpContext.Session.GetInt32("RoleId") != 1)
            {
                return RedirectToPage("/Index");
            }
            if (id == null || _context.ServiceCategories == null)
            {
                return NotFound();
            }

            var servicecategory =  await _context.ServiceCategories.FirstOrDefaultAsync(m => m.Id == id);
            if (servicecategory == null)
            {
                return NotFound();
            }
            ServiceCategory = servicecategory;
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(ServiceCategory).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ServiceCategoryExists(ServiceCategory.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool ServiceCategoryExists(int id)
        {
          return (_context.ServiceCategories?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
