﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using SWP391_G1.Models;

namespace SWP391_G1.Pages.Admin.ServiceCategories
{
    public class CreateModel : PageModel
    {
        private readonly SWP391_G1.Models.SakuraCosmeticContext _context;

        public CreateModel(SWP391_G1.Models.SakuraCosmeticContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            if (HttpContext.Session.GetInt32("id") == null && HttpContext.Session.GetInt32("RoleId") != null && HttpContext.Session.GetInt32("RoleId") != 1)
            {
                return RedirectToPage("/Index");
            }
            return Page();
        }

        [BindProperty]
        public ServiceCategory ServiceCategory { get; set; } = default!;
        

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
          if (!ModelState.IsValid || _context.ServiceCategories == null || ServiceCategory == null)
            {
                return Page();
            }

            _context.ServiceCategories.Add(ServiceCategory);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
