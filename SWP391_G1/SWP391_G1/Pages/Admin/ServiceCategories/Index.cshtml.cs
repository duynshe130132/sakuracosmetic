using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SWP391_G1.Models;
using SWP391_G1.Services;

namespace SWP391_G1.Pages.Admin.ServiceCategories
{
    public class IndexModel : PageModel
    {
        public static SakuraCosmeticContext context;
        public IndexModel (SakuraCosmeticContext _context) { 
            context = _context;
        }
        [BindProperty(SupportsGet = true)]
        public string? SearchString { get; set; }
        [BindProperty(Name = "id", SupportsGet = true)]
        public int Id { get; set; }
        [BindProperty(SupportsGet = true)]
        public IList<ServiceCategory> ServiceCategory { get; set; } = default;
        public IActionResult OnGet(string searchString)
        {
            if (HttpContext.Session.GetInt32("id") == null && HttpContext.Session.GetInt32("RoleId") != null && HttpContext.Session.GetInt32("RoleId") != 1)
            {
                return RedirectToPage("/Index");
            }
            if (HttpContext.Session.GetInt32("id") == null)
            {
                return RedirectToPage("/Index");
            }
            var listCategories = String.IsNullOrEmpty(searchString) ? context.ServiceCategories.ToList() : context.ServiceCategories.Where(x => x.CateName.Contains(searchString)).ToList();
            ServiceCategory = listCategories;
            return Page();
        }
        public IActionResult OnPostDisableService(int id)
        {
            var serviceToUpdate = context.ServiceCategories.FirstOrDefault(x => x.Id == id);
            if (serviceToUpdate != null)
            {
                serviceToUpdate.Status = false;
                context.SaveChanges();
            }
            TempData["ToastMessage"] = "Service Category disabled successfully.";
            TempData["ToastType"] = "success";
            return RedirectToPage("/Admin/ServiceCategories/Index");
        }

        public IActionResult OnPostEnableService(int id)
        {
            var serviceToUpdate = context.ServiceCategories.FirstOrDefault(x => x.Id == id);
            if (serviceToUpdate != null)
            {
                serviceToUpdate.Status = true;
                context.SaveChanges();
            }
            TempData["ToastMessage"] = "Service Category enabled successfully.";
            TempData["ToastType"] = "success";
            return RedirectToPage("/Admin/ServiceCategories/Index");
        }
    }
}
