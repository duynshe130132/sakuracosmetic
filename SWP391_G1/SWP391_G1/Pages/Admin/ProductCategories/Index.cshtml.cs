﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using SWP391_G1.Models;

namespace SWP391_G1.Pages.Admin.ProductCategories
{
    public class IndexModel : PageModel
    {
        public static SakuraCosmeticContext context;
        public IndexModel(SakuraCosmeticContext _context) {
            context = _context;
        }
        [BindProperty(SupportsGet = true)]
        public string? SearchString { get; set; }
        [BindProperty(Name = "id", SupportsGet = true)]
        public int Id { get; set; }
        [BindProperty(SupportsGet = true)]
        public IList<Category> ProductCategory { get; set; } = default;
        public IActionResult OnGet(string searchString)
        {
            if (HttpContext.Session.GetInt32("id") == null)
            {
                return RedirectToPage("/Index");
            }
            var listCategories = String.IsNullOrEmpty(searchString) ? context.Categories.ToList() : context.Categories.Where(x => x.CategoryName.Contains(searchString)).ToList();
            ProductCategory = listCategories;
            return Page();
        }
        //public IActionResult OnPostDisableService(int id)
        //{
        //    var serviceToUpdate = context.Categories.FirstOrDefault(x => x.CategoryId == id);
        //    if (serviceToUpdate != null)
        //    {
        //        serviceToUpdate.Status = false;
        //        context.SaveChanges();
        //    }
        //    TempData["ToastMessage"] = "Service Category disabled successfully.";
        //    TempData["ToastType"] = "success";
        //    return RedirectToPage("/Admin/ServiceCategories/Index");
        //}

        //public IActionResult OnPostEnableService(int id)
        //{
        //    var serviceToUpdate = context.ServiceCategories.FirstOrDefault(x => x.Id == id);
        //    if (serviceToUpdate != null)
        //    {
        //        serviceToUpdate.Status = true;
        //        context.SaveChanges();
        //    }
        //    TempData["ToastMessage"] = "Service Category enabled successfully.";
        //    TempData["ToastType"] = "success";
        //    return RedirectToPage("/Admin/ServiceCategories/Index");
        //}
    }
}
