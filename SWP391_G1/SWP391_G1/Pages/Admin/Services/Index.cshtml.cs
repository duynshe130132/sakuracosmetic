﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SWP391_G1.Models;
using SWP391_G1.Services;

namespace SWP391_G1.Pages.Admin.Services
{
    public class IndexModel : PageModel
    {
        public static SakuraCosmeticContext context;
        
        //PaginatedList<Service> services = new PaginatedList<Service>(context.Services.Include(x => x.Cate).ToList(), context.Services.Count(), 1, 10);
        
        public IndexModel(SakuraCosmeticContext _context) {
            context= _context;
        }
        [BindProperty(SupportsGet = true)]
        public string? SearchString { get; set; }

        public SelectList? Categories { get; set; }

        [BindProperty(SupportsGet = true)]
        public int CategoryId { get; set; } = 0;

        [BindProperty(SupportsGet = true)]
        public int IndexPaging { get; set; } = 1;

        [BindProperty(Name = "id", SupportsGet = true)]
        public int Id { get; set; }

        public int TotalPage { get; set; }
        [BindProperty(SupportsGet = true)]
        public Service Service { get; set; }
        public int ServiceCount { get; set; }
        [BindProperty(SupportsGet = true)]
        public IList<ServiceCategory> ServiceCategory { get; set; } = default;
        [BindProperty(SupportsGet = true)]
        public IList<Service> Services { get; set; } = default;
        [BindProperty(SupportsGet = true)]
        public int PageSize { get; set; } = default;
        public IActionResult OnGet(int categoryId, string searchString, int indexPaging, int pageSize)
        {
           if(HttpContext.Session.GetInt32("id") == null && HttpContext.Session.GetInt32("RoleId") != null && HttpContext.Session.GetInt32("RoleId") != 1)
            {
                return RedirectToPage("/Index");
            }
            PaginatedList<Service> services;
            if (pageSize == 0)
            {
                pageSize = context.Services.ToList().Count;
            }
            if (categoryId != 0)
            {
                var listService = String.IsNullOrEmpty(searchString) ? context.Services.Include(x => x.Cate).Where(x => x.CateId == categoryId ).ToList() : context.Services.Include(x => x.Cate).Where(x => x.CateId == categoryId && x.ServiceName.Contains(searchString)).ToList();
                services = new PaginatedList<Service>(listService, listService.Count, 1, pageSize);
            }
            else
            {
                var listService = String.IsNullOrEmpty(searchString) ? context.Services.Include(x => x.Cate).ToList() : context.Services.Include(x => x.Cate).Where(x => x.ServiceName.Contains(searchString)).ToList();
                services = new PaginatedList<Service>(listService, listService.Count, 1, pageSize);
            }

            TotalPage = services.TotalPages;
            List<ServiceCategory> serviceCategory = context.ServiceCategories.ToList();
            ServiceCount = context.Services.ToList().Count;
            ServiceCategory = serviceCategory;
            ViewData["categoryList"] = serviceCategory;
            ViewData["CategoryId"] = new SelectList(serviceCategory, "CategoryId", "CategoryName");
            Services = PaginatedList<Service>.Create(services.AsQueryable<Service>(), indexPaging, pageSize);

            return Page();
        }
        public async Task<IActionResult> OnPost()
        {
            context.Services.Add(Service);
            context.SaveChanges();
            return RedirectToPage("./Index");
        }
        public IActionResult OnPostDisableService(int id)
        {
            var serviceToUpdate = context.Services.FirstOrDefault(x => x.ServiceId == id);
            if (serviceToUpdate != null)
            {
                serviceToUpdate.Status = false;
                context.SaveChanges();
            }
            TempData["ToastMessage"] = "Service disabled successfully.";
            TempData["ToastType"] = "success";
            return RedirectToPage("/Admin/Services/Index");
        }

        public IActionResult OnPostEnableService(int id)
        {
            var serviceToUpdate = context.Services.FirstOrDefault(x => x.ServiceId == id);
            if (serviceToUpdate != null)
            {
                serviceToUpdate.Status = true;
                context.SaveChanges();
            }
            TempData["ToastMessage"] = "Service enabled successfully.";
            TempData["ToastType"] = "success";
            return RedirectToPage("/Admin/Services/Index");
        }

        
    }
}
