using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using SWP391_G1.Models;
using SWP391_G1.Services;
using static System.Net.WebRequestMethods;

namespace SWP391_G1.Pages.Admin.Services
{
    public class EditModel : PageModel
    {
        SakuraCosmeticContext _context = new SakuraCosmeticContext();
        private readonly FlieUploadService uploadservice = new FlieUploadService();
        [BindProperty(SupportsGet = true)]
        public Service service { get; set; } = default!;
        [BindProperty(SupportsGet = true)]
        public List<ServiceCategory> listCategory { get; set; } = default!;
        [BindProperty(SupportsGet = true)]
        public string ServiceDescription { get; set; }
        [BindProperty(SupportsGet = true)]
        public string PreImageurl { get; set; }
        public string ErrorValidImage { get; set; }
        public async Task<IActionResult> OnGet(int? id)
        {
            if (HttpContext.Session.GetInt32("id") == null && HttpContext.Session.GetInt32("RoleId") != null && HttpContext.Session.GetInt32("RoleId") != 1)
            {
                return RedirectToPage("/Index");
            }
            service = _context.Services.FirstOrDefault(x => x.ServiceId == id);
            PreImageurl = "https://drive.google.com/uc?id=" + service.ServiceImage;
            listCategory = _context.ServiceCategories.ToList();
            ViewData["CategoryId"] = new SelectList(_context.ServiceCategories, "Id", "CateName");
            return Page();
        }
        public async Task<IActionResult> OnPost(IFormFile file)
        {
            
            service.Description = ServiceDescription;
            if (file != null)
            {
                if (file.Length > (25 * 1024 * 1024) || !file.ContentType.StartsWith("image/"))
                {
                    ErrorValidImage = "Image Is Invalid.";
                    return Page();
                }
                bool isDelete = await uploadservice.DeleteFile(service.ServiceImage);
                var filePath = await uploadservice.DriveUploadBasic(file);
                service.ServiceImage = filePath;
            }       
            _context.Entry<Service>(service).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            _context.SaveChanges();
            return RedirectToPage("/Admin/Services/Index");
        }
    }
}
