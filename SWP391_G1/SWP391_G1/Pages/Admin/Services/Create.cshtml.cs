﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using SWP391_G1.Models;
using SWP391_G1.Services;
using static System.Net.WebRequestMethods;

namespace SWP391_G1.Pages.Admin.Services
{
    public class CreateModel : PageModel
    {
        private readonly SWP391_G1.Models.SakuraCosmeticContext _context;
        private readonly FlieUploadService service = new FlieUploadService();
        public CreateModel(SWP391_G1.Models.SakuraCosmeticContext context)
        {
            _context = context;
        }
        [BindProperty(SupportsGet =true)]
        public string fileName { get; set; }
        public string filePath { get; set; } = default!;
        [BindProperty(SupportsGet = true)]
        public string ServiceDescription { get; set; }
        public string ErrorValidImage { get; set; }
        public IActionResult OnGet()
        {
            if (HttpContext.Session.GetInt32("id") == null && HttpContext.Session.GetInt32("RoleId") != null && HttpContext.Session.GetInt32("RoleId") != 1)
            {
                return RedirectToPage("/Index");
            }
            ViewData["CateId"] = new SelectList(_context.ServiceCategories, "Id", "CateName");
            return Page();
        }

        [BindProperty(SupportsGet = true)]
        public Service Service { get; set; } = default!;
        

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync(IFormFile file)
        {
          if (!ModelState.IsValid || _context.Services == null || Service == null)
            {
                return Page();
            }
            if (file.Length > (25 * 1024 * 1024) || !file.ContentType.StartsWith("image/"))
            {
                ErrorValidImage = "Image Is Invalid.";
                return Page();
            }
            if (file != null)
            {
                fileName= file.FileName ;
                filePath = await service.DriveUploadBasic(file);
                Service.Description = ServiceDescription;
                Service.ServiceImage = filePath;
            }
            else
            {
                return Page();
            }
            _context.Services.Add(Service);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
