using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SWP391_G1.Models;
using SWP391_G1.Services;

namespace SWP391_G1.Pages.Admin.Employees
{
    public class IndexModel : PageModel
    {
        public static SakuraCosmeticContext context;
        public IndexModel(SakuraCosmeticContext _context)
        {
            context = _context;
        }
        //PaginatedList<Employee> employees = new PaginatedList<Employee>(context.Employees.Include(x => x.Role).ToList(), context.Employees.Count(), 1, 10);
        [BindProperty(SupportsGet = true)]
        public string? SearchString { get; set; }

        [BindProperty(SupportsGet = true)]
        public int IndexPaging { get; set; } = 1;

        [BindProperty(Name = "id", SupportsGet = true)]
        public int Id { get; set; }

        public int TotalPage { get; set; }
        public int EmployeeCount { get; set; }
        [BindProperty(SupportsGet = true)]
        public IList<Employee> Employees { get; set; } = default;
        [BindProperty(SupportsGet = true)]
        public int PageSize { get; set; } = default;
        public IActionResult OnGet(string searchString, int indexPaging, int pageSize)
        {
            if (HttpContext.Session.GetInt32("id") == null && HttpContext.Session.GetInt32("RoleId") != null && HttpContext.Session.GetInt32("RoleId") != 1)
            {
                return RedirectToPage("/Index");
            }
            PaginatedList<Employee> employees;
            if (pageSize == 0)
            {
                pageSize = context.Employees.ToList().Count;
            }
            var listEmployee = String.IsNullOrEmpty(searchString) ? context.Employees.Include(x => x.Role).Where(x => x.RoleId == 2).ToList() : context.Employees.Include(x => x.Role).Where(x => x.FullName.Contains(searchString) || x.UserName.Contains(searchString) && x.RoleId == 2).ToList();
            employees = new PaginatedList<Employee>(listEmployee, listEmployee.Count, 1, pageSize);

            TotalPage = employees.TotalPages;
            EmployeeCount = context.Employees.ToList().Count;
            Employees = PaginatedList<Employee>.Create(employees.AsQueryable<Employee>(), indexPaging, pageSize);
            return Page();
        }
        public IActionResult OnPostDisableEmployee(int id)
        {
            var EmployeeToUpdate = context.Employees.FirstOrDefault(x => x.Id == id);
            if (EmployeeToUpdate != null)
            {
                EmployeeToUpdate.Status = false;
                context.SaveChanges();
            }
            TempData["ToastMessage"] = "Employee disabled successfully.";
            TempData["ToastType"] = "success";
            return RedirectToPage("/Admin/Employees/Index");
        }

        public IActionResult OnPostEnableEmployee(int id)
        {
            var EmployeeToUpdate = context.Employees.FirstOrDefault(x => x.Id == id);
            if (EmployeeToUpdate != null)
            {
                EmployeeToUpdate.Status = true;
                context.SaveChanges();
            }
            TempData["ToastMessage"] = "Employee enabled successfully.";
            TempData["ToastType"] = "success";
            return RedirectToPage("/Admin/Employees/Index");
        }
    }
}
