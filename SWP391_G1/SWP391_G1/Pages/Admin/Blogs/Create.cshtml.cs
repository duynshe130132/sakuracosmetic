﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using SWP391_G1.Models;
using SWP391_G1.Services;
namespace SWP391_G1.Pages.Admin.Blogs
{
    public class CreateModel : PageModel
    {
        private readonly SWP391_G1.Models.SakuraCosmeticContext _context;
        private readonly FlieUploadService uploadservice = new FlieUploadService();
        public CreateModel(SWP391_G1.Models.SakuraCosmeticContext context)
        {
            _context = context;
        }
        [BindProperty(SupportsGet =true)]
        public string BlogDescription { get; set; }
        public IActionResult OnGet()
        {
            if (HttpContext.Session.GetInt32("id") == null && HttpContext.Session.GetInt32("RoleId") != null && HttpContext.Session.GetInt32("RoleId") != 1)
            {
                return RedirectToPage("/Index");
            }
            ViewData["CateId"] = new SelectList(_context.BlogCategories, "Id", "CateName");
            return Page();
        }

        [BindProperty]
        public Blog Blog { get; set; } = default!;
        

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync(IFormFile file)
        {
          if (!ModelState.IsValid || _context.Blogs == null || Blog == null)
            {
                return Page();
            }
            Blog.Description = BlogDescription;
            var filePath = await uploadservice.DriveUploadBasic(file);
            Blog.BlogImageUrl = filePath;
            _context.Blogs.Add(Blog);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
