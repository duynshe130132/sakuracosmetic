﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SWP391_G1.Models;
using SWP391_G1.Services;

namespace SWP391_G1.Pages.Admin.Blogs
{
    public class IndexModel : PageModel
    {
        public static SakuraCosmeticContext context;
        public IndexModel(SakuraCosmeticContext _context)
        {
            context = _context;
        }
        //PaginatedList<Blog> blogs = new PaginatedList<Blog>(context.Blogs.Include(x => x.Cate).ToList(), context.Blogs.Count(), 1, 10);
        [BindProperty(SupportsGet = true)]
        public string? SearchString { get; set; }

        public SelectList? Categories { get; set; }

        [BindProperty(SupportsGet = true)]
        public int CategoryId { get; set; } = 0;

        [BindProperty(SupportsGet = true)]
        public int IndexPaging { get; set; } = 1;

        [BindProperty(Name = "id", SupportsGet = true)]
        public int Id { get; set; }

        public int TotalPage { get; set; }
        public int BlogCount { get; set; }
        [BindProperty(SupportsGet = true)]
        public IList<BlogCategory> BlogCategory { get; set; } = default;
        [BindProperty(SupportsGet = true)]
        public List<Blog> Blogs { get; set; } = default;
        [BindProperty(SupportsGet = true)]
        public int PageSize { get; set; } = default;
        public IActionResult OnGet(int categoryId, string searchString, int indexPaging, int pageSize)
        {
            if (HttpContext.Session.GetInt32("id") == null && HttpContext.Session.GetInt32("RoleId") != null && HttpContext.Session.GetInt32("RoleId") != 1)
            {
                return RedirectToPage("/Index");
            }
            PaginatedList<Blog> blogs;
            if (pageSize == 0)
            {
                pageSize = context.Services.ToList().Count;
            }
            if (categoryId != 0)
            {
                var listBlog = String.IsNullOrEmpty(searchString) ? context.Blogs.Include(x => x.Cate).Where(x => x.CateId == categoryId).ToList() : context.Blogs.Include(x => x.Cate).Where(x => x.CateId == categoryId && (x.BlogTitle.Contains(searchString) || x.Description.Contains(searchString))).ToList();
                blogs = new PaginatedList<Blog>(listBlog, listBlog.Count, 1, pageSize);
            }
            else
            {
                var listBlog = String.IsNullOrEmpty(searchString) ? context.Blogs.Include(x => x.Cate).ToList() : context.Blogs.Include(x => x.Cate).Where(x => x.BlogTitle.Contains(searchString) || x.Description.Contains(searchString)).ToList();
                blogs = new PaginatedList<Blog>(listBlog, listBlog.Count, 1, pageSize);
            }

            TotalPage = blogs.TotalPages;
            List<BlogCategory> blogCategory = context.BlogCategories.ToList();
            BlogCount = context.Blogs.ToList().Count;
            BlogCategory = blogCategory;
            ViewData["categoryList"] = blogCategory;
            ViewData["CategoryId"] = new SelectList(blogCategory, "CategoryId", "CategoryName");
            Blogs = PaginatedList<Blog>.Create(blogs.AsQueryable<Blog>(), indexPaging, pageSize);
            return Page();
        }
    }
}
