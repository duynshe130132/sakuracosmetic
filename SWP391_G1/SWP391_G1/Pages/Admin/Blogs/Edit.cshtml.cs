﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SWP391_G1.Models;
using SWP391_G1.Services;

namespace SWP391_G1.Pages.Admin.Blogs
{
    public class EditModel : PageModel
    {
        private readonly SWP391_G1.Models.SakuraCosmeticContext _context;
        private readonly FlieUploadService uploadservice = new FlieUploadService();
        public EditModel(SWP391_G1.Models.SakuraCosmeticContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Blog Blog { get; set; } = default!;
        [BindProperty(SupportsGet = true)]
        public string BlogDescription { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null || _context.Blogs == null)
            {
                return NotFound();
            }

            var blog =  await _context.Blogs.Include(x => x.Cate).FirstOrDefaultAsync(m => m.BlogId == id);
            if (blog == null)
            {
                return NotFound();
            }
            Blog = blog;
           ViewData["CateId"] = new SelectList(_context.BlogCategories, "Id", "CateName");
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync(IFormFile file)
        {
            if (HttpContext.Session.GetInt32("id") == null && HttpContext.Session.GetInt32("RoleId") != null && HttpContext.Session.GetInt32("RoleId") != 1)
            {
                return RedirectToPage("/Index");
            }
            Blog.Description = BlogDescription;
            var filePath = await uploadservice.DriveUploadBasic(file);
            Blog.BlogImageUrl = filePath;
            Blog.Cate = _context.BlogCategories.FirstOrDefault(x => x.Id == Blog.CateId);
            _context.Attach(Blog).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BlogExists(Blog.BlogId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool BlogExists(int id)
        {
          return (_context.Blogs?.Any(e => e.BlogId == id)).GetValueOrDefault();
        }
    }
}
