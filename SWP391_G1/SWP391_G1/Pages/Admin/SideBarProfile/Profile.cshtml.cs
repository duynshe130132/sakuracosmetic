using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SWP391_G1.Models;

namespace SWP391_G1.Pages.Admin.SideBarProfile
{
    public class ProfileModel : PageModel
    {
        private readonly SWP391_G1.Models.SakuraCosmeticContext context;

        public ProfileModel(SWP391_G1.Models.SakuraCosmeticContext Context)
        {
            context = Context;
        }

        [BindProperty]
        public Employee Employee { get; set; } = default!;
        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (HttpContext.Session.GetInt32("id") == null)
            {
                return RedirectToPage("/Index");
            }
            var employee =context.Employees.SingleOrDefault(e=>e.Id == HttpContext.Session.GetInt32("id"));
            if(employee == null)
            {
                return NotFound();
            }
            Employee = employee;
            return Page();
        }

        public IActionResult OnPost()
        {
            var employee = context.Employees.SingleOrDefault(e => e.Id == HttpContext.Session.GetInt32("id"));


            if (!ModelState.IsValid)
            {
                return Page();
            }
            employee.FullName = Employee.FullName;
            employee.Address = Employee.Address;
            employee.City = Employee.City;
            employee.State = Employee.State;
            employee.Country = Employee.Country;
            employee.Phone = Employee.Phone;
            employee.Email = Employee.Email;
            //context.Attach(Employee).State = EntityState.Modified;

            context.SaveChanges();

            HttpContext.Session.SetString("UpdateSuccess", "Update Success !!! ");
            return Page();
           

        }
       
    }
}
