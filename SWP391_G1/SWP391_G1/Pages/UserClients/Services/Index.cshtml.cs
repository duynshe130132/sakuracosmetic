using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SWP391_G1.Models;
using SWP391_G1.Services;
using System.Text.Json;

namespace SWP391_G1.Pages.UserClients.Services
{
    public class IndexModel : PageModel
    {
        public static SakuraCosmeticContext context = new SakuraCosmeticContext();
        PaginatedList<Service> services = new PaginatedList<Service>(context.Services.Include(x => x.Cate).ToList(), context.Services.Count(), 1, 6);
        [BindProperty(SupportsGet = true)]
        public string? SearchString { get; set; }

        public SelectList? Categories { get; set; }

        [BindProperty(SupportsGet = true)]
        public int CategoryId { get; set; } = 0;

        [BindProperty(SupportsGet = true)]
        public int IndexPaging { get; set; } = 1;

        [BindProperty(Name = "id", SupportsGet = true)]
        public int Id { get; set; }

        public int TotalPage { get; set; }
        [BindProperty(SupportsGet = true)]
        public Service Service { get; set; }
        public int ServiceCount { get; set; }
        [BindProperty(SupportsGet = true)]
        public IList<ServiceCategory> ServiceCategory { get; set; } = default;
        [BindProperty(SupportsGet = true)]
        public IList<Service> Services { get; set; } = default!;
        [BindProperty(SupportsGet = true)]
        public int PageSize { get; set; } = default;
        [BindProperty(SupportsGet = true)]
        public ReservationCartDTO cart { get; set; } = default!;
        public List<ReservationCartDTO> listcarts { get; set; } = new List<ReservationCartDTO>();
        public void OnGet(int categoryId, string searchString, int indexPaging)
        {

            //PaginatedList<Service> services;
            if (categoryId != 0)
            {
                var listService = String.IsNullOrEmpty(searchString) ? context.Services.Include(x => x.Cate).Where(x => x.CateId == categoryId && x.Status == true).ToList() : context.Services.Include(x => x.Cate).Where(x => x.CateId == categoryId && x.ServiceName.Contains(searchString) && x.Status == true).ToList();
                services = new PaginatedList<Service>(listService, listService.Count, 1, 6);
            }
            else
            {
                var listService = String.IsNullOrEmpty(searchString) ? context.Services.Include(x => x.Cate).Where(x=> x.Status == true).ToList() : context.Services.Include(x => x.Cate).Where(x => x.ServiceName.Contains(searchString) && x.Status == true).ToList();
                services = new PaginatedList<Service>(listService, listService.Count, 1, 6);
            }

            TotalPage = services.TotalPages;
            List<ServiceCategory> serviceCategory = context.ServiceCategories.ToList();
            ServiceCount = context.Services.ToList().Count;
            ServiceCategory = serviceCategory;
            ViewData["categoryList"] = serviceCategory;
            ViewData["CategoryId"] = new SelectList(serviceCategory, "CategoryId", "CategoryName");
            Services = PaginatedList<Service>.Create(services.AsQueryable<Service>(), indexPaging, 6);
            ViewData["StaffId"] = new SelectList(context.Employees, "Id", "FullName");
        }
        public IActionResult OnPostAddToCart()
        {
            if(HttpContext.Session.GetInt32("id") == null)
            {
                return RedirectToPage("/Auth/Login");
            }
            if (HttpContext.Session.GetString("ReservationCarts") == null)
            {
                cart.RecordId = 1;
                cart.DateCreated = DateTime.Now;
                listcarts.Add(cart);
                HttpContext.Session.SetString("ReservationCarts", JsonSerializer.Serialize(listcarts));
            } else
            {
                var oldlistcart = JsonSerializer.Deserialize<List<ReservationCartDTO>>(HttpContext.Session.GetString("ReservationCarts"));
                if(oldlistcart.Count <= 0)
                {
                    cart.RecordId = 1;               
                }
                else
                {
                    cart.RecordId = oldlistcart[oldlistcart.Count - 1].RecordId + 1;
                }
                cart.DateCreated = DateTime.Now;
                oldlistcart.Add(cart);
                HttpContext.Session.SetString("ReservationCarts", JsonSerializer.Serialize(oldlistcart));
            }
            return RedirectToPage("/UserClients/Services/Index");
        }
    }
}
