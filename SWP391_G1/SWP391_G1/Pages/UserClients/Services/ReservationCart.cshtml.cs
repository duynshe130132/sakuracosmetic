using Google.Apis.Drive.v3.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using SWP391_G1.Models;
using SWP391_G1.Services;
using SWP391_G1.VnPaymentService;
using System.Text.Json;
using System.Text.RegularExpressions;

namespace SWP391_G1.Pages.UserClients.Services
{
    public class ReservationCartModel : PageModel
    {
        private readonly SakuraCosmeticContext _context;
        private readonly PaymentService _vnPayService = null;
        public ReservationCartModel(SakuraCosmeticContext context, PaymentService vnPayService)
        {
            _context = context;
            _vnPayService = vnPayService;
        }

        [BindProperty(SupportsGet = true)]
        public decimal total { get; set; }
        [BindProperty(SupportsGet = true)]
        public int countItem { get; set; }
        [BindProperty(SupportsGet = true)]
        public List<ReservationCartDTO> Cart { get; set; } = default!;
        [BindProperty(SupportsGet = true)]
        public DateTime dateBook { get; set; }
        public async Task<IActionResult> OnGet()
        {
            if (HttpContext.Session.GetInt32("id") == null)
            {
                Response.Redirect("/Auth/Login");
            }
            if(HttpContext.Session.GetString("ReservationCarts") == null)
            {
                return Page();
            }
            Cart = JsonSerializer.Deserialize<List<ReservationCartDTO>>(HttpContext.Session.GetString("ReservationCarts"));
            total = GetTotal();
            countItem = GetCount();
            HttpContext.Session.SetString("Total", total.ToString());
            ViewData["Total"] = total.ToString();
            return Page();
        }
        public async Task<IActionResult> OnPostUpdateDate(int? id)
        {
            int index = Cart.FindIndex(x => x.RecordId == id);
            if (index != -1)
            {
                Cart[index].ReservationTime = dateBook;
            }
            HttpContext.Session.SetString("ReservationCarts", JsonSerializer.Serialize(Cart));
            return RedirectToPage("/UserClients/Services/ReservationCart");
        }
        public async Task<IActionResult> OnPostRemove(int? id)
        {
            Cart = JsonSerializer.Deserialize<List<ReservationCartDTO>>(HttpContext.Session.GetString("ReservationCarts"));
            int index = Cart.FindIndex(x => x.RecordId == id);
            if (index != -1)
            {
                Cart.Remove(Cart[index]);
            }
            HttpContext.Session.SetString("ReservationCarts", JsonSerializer.Serialize(Cart));
            return RedirectToPage("/UserClients/Services/ReservationCart");
        }
        public decimal GetTotal()
        {
            // Multiply album price by count of that album to get
            // the current price for each of those albums in the cart
            // sum all album price totals to get the cart total

            decimal? total = 0;
            foreach(var item in Cart)
            {
                Service service = _context.Services.FirstOrDefault(x => x.ServiceId == item.ServiceId);
                total += service.Price;
            }
            return total ?? 0;
        }
        public int GetCount()
        {
            int? count = Cart.Count;
            // Return 0 if all entries are null
            return count ?? 0;

        }
        public async Task<IActionResult> OnPostCheckout()
        {
            //-----------------------------Payment----------------------//
            HttpContext.Session.SetString("PaymentType", "Service");
            PaymentInformationModel model = new PaymentInformationModel();
            model.OrderType = "Pay Order Service";
            model.Amount = double.Parse(HttpContext.Session.GetString("Total"));
            model.OrderDescription = "Pay Order Service";
            model.Name = HttpContext.Session.GetString("username");

            string url = _vnPayService.CreatePaymentUrl(model, HttpContext);

            return Redirect(url);
        }
    }
}
