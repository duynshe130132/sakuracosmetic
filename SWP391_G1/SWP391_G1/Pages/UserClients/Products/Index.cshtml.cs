using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SWP391_G1.Models;
using SWP391_G1.Services;
using System.Text.Json;

namespace SWP391_G1.Pages.UserClients.Products
{
    public class IndexModel : PageModel
    {
        public static SakuraCosmeticContext context = new SakuraCosmeticContext();
        public IndexModel (SakuraCosmeticContext _context) {
            context = _context;
        }
        //PaginatedList<Product> products = new PaginatedList<Product>(context.Products.Include(x => x.Category).ToList(), context.Products.Count(), 1, 8);
        [BindProperty(SupportsGet = true)]
        public string? SearchString { get; set; }

        public SelectList? Categories { get; set; }

        [BindProperty(SupportsGet = true)]
        public int CategoryId { get; set; } = 0;

        [BindProperty(SupportsGet = true)]
        public int IndexPaging { get; set; } = 1;

        [BindProperty(Name = "id", SupportsGet = true)]
        public int Id { get; set; }

        public int TotalPage { get; set; }
        [BindProperty(SupportsGet = true)]
        public Product Product { get; set; }
        public int ProductCount { get; set; }
        [BindProperty(SupportsGet = true)]
        public IList<Category> ProductCategory { get; set; } = default;
        [BindProperty(SupportsGet = true)]
        public IList<Product> Products { get; set; } = default!;
        [BindProperty(SupportsGet = true)]
        public int PageSize { get; set; } = default;
        [BindProperty(SupportsGet = true)]
        public CartDTO cart { get; set; } = default!;
        public List<CartDTO> listcarts { get; set; } = new List<CartDTO>();
        public void OnGet(int categoryId, string searchString, int indexPaging)
        {

            PaginatedList<Product> products;
            if (categoryId != 0)
            {
                var listProduct = String.IsNullOrEmpty(searchString) ? context.Products.Include(x => x.Category).Where(x => x.CategoryId == categoryId && x.ProductStatus == 1).ToList() : context.Products.Include(x => x.Category).Where(x => x.CategoryId == categoryId && x.ProductName.Contains(searchString) && x.ProductStatus == 1).ToList();
                products = new PaginatedList<Product>(listProduct, listProduct.Count, 1, 8);
            }
            else
            {
                var listProduct = String.IsNullOrEmpty(searchString) ? context.Products.Include(x => x.Category).Where(x => x.ProductStatus == 1).ToList() : context.Products.Include(x => x.Category).Where(x => x.ProductName.Contains(searchString) && x.ProductStatus == 1).ToList();
                products = new PaginatedList<Product>(listProduct, listProduct.Count, 1, 8);
            }

            TotalPage = products.TotalPages;
            List<Category> productCategory = context.Categories.ToList();
            ProductCount = context.Services.ToList().Count;
            ProductCategory = productCategory;
            ViewData["categoryList"] = productCategory;
            ViewData["CategoryId"] = new SelectList(productCategory, "CategoryId", "CategoryName");
            Products = PaginatedList<Product>.Create(products.AsQueryable<Product>(), indexPaging, 8);
        }
        public IActionResult OnPostAddToCart(int id)
        {
            if (HttpContext.Session.GetInt32("id") == null)
            {
                return RedirectToPage("/Auth/Login");
            }
            if (HttpContext.Session.GetString("ProductCarts") == null)
            {
                cart.RecordId = 1;
                cart.DateCreated = DateTime.Now;
                cart.ProductId = id;
                cart.Count = 1;
                listcarts.Add(cart);
                HttpContext.Session.SetString("ProductCarts", JsonSerializer.Serialize(listcarts));
            }
            else
            {
                var oldlistcart = JsonSerializer.Deserialize<List<CartDTO>>(HttpContext.Session.GetString("ProductCarts"));
                if (oldlistcart.Count <= 0)
                {
                    cart.RecordId = 1;
                    cart.DateCreated = DateTime.Now;
                    cart.ProductId = id;
                    cart.Count = 1;
                    oldlistcart.Add(cart);
                    
                }
                else
                {
                    CartDTO oldcart = oldlistcart.FirstOrDefault(x => x.ProductId == id);
                    if (oldcart != null)
                    {
                        oldcart.Count += 1;
                        cart.DateCreated = DateTime.Now;
                    }
                    else
                    {
                        cart.RecordId = oldlistcart[oldlistcart.Count - 1].RecordId + 1;
                        cart.DateCreated = DateTime.Now;
                        cart.ProductId = id;
                        cart.Count = 1;
                        oldlistcart.Add(cart);
                    }
                    
                }
                HttpContext.Session.SetString("ProductCarts", JsonSerializer.Serialize(oldlistcart));
            }
            return RedirectToPage("/UserClients/Products/Index");
        }
    }
}
