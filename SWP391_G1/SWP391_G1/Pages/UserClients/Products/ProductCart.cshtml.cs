using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SWP391_G1.Models;
using SWP391_G1.Services;
using SWP391_G1.VnPaymentService;
using System.Text.Json;

namespace SWP391_G1.Pages.UserClients.Products
{
    public class ProductCartModel : PageModel
    {
        private readonly SakuraCosmeticContext _context;
        private readonly PaymentService _vnPayService = null;
        public ProductCartModel(SakuraCosmeticContext context, PaymentService vnPayService)
        {
            _context = context;
            _vnPayService = vnPayService;
        }

        [BindProperty(SupportsGet = true)]
        public decimal total { get; set; }
        [BindProperty(SupportsGet = true)]
        public int countItem { get; set; }
        [BindProperty(SupportsGet = true)]
        public List<CartDTO> Cart { get; set; } = default!;
        public async Task<IActionResult> OnGet()
        {
            if (HttpContext.Session.GetInt32("id") == null)
            {
                Response.Redirect("/Auth/Login");
            }
            if (HttpContext.Session.GetString("ProductCarts") == null)
            {
                return Page();
            }
            Cart = JsonSerializer.Deserialize<List<CartDTO>>(HttpContext.Session.GetString("ProductCarts"));
            total = GetTotal();
            countItem = GetCount();
            HttpContext.Session.SetString("Total", total.ToString());
            ViewData["Total"] = total.ToString();
            return Page();
        }
        public async Task<IActionResult> OnPostIncreaseFromCart(int? id)
        {
            Cart = JsonSerializer.Deserialize<List<CartDTO>>(HttpContext.Session.GetString("ProductCarts"));
            int index = Cart.FindIndex(x => x.RecordId == id);
            if (index != -1)
            {
                Cart[index].Count += 1;
            }
            HttpContext.Session.SetString("ProductCarts", JsonSerializer.Serialize(Cart));
            return RedirectToPage("/UserClients/Products/ProductCart");
        }
        public async Task<IActionResult> OnPostRemoveFromCart(int? id)
        {
            Cart = JsonSerializer.Deserialize<List<CartDTO>>(HttpContext.Session.GetString("ProductCarts"));
            int index = Cart.FindIndex(x => x.RecordId == id);
            if (index != -1)
            {
                Cart[index].Count -= 1;
            }
            if(Cart[index].Count == 0)
            {
                Cart.Remove(Cart[index]);
            }
            HttpContext.Session.SetString("ProductCarts", JsonSerializer.Serialize(Cart));
            return RedirectToPage("/UserClients/Products/ProductCart");
        }
        public async Task<IActionResult> OnPostRemove(int? id)
        {
            Cart = JsonSerializer.Deserialize<List<CartDTO>>(HttpContext.Session.GetString("ProductCarts"));
            int index = Cart.FindIndex(x => x.RecordId == id);
            if (index != -1)
            {
                Cart.Remove(Cart[index]);
            }
            HttpContext.Session.SetString("ProductCarts", JsonSerializer.Serialize(Cart));
            return RedirectToPage("/UserClients/Products/ProductCart");
        }
        public decimal GetTotal()
        {
            // Multiply album price by count of that album to get
            // the current price for each of those albums in the cart
            // sum all album price totals to get the cart total

            decimal? total = 0;
            foreach (var item in Cart)
            {
                Product product = _context.Products.FirstOrDefault(x => x.ProductId == item.ProductId);
                total += product.ProductPrice*item.Count;
            }
            return total ?? 0;
        }
        public int GetCount()
        {
            int? count = Cart.Count;
            // Return 0 if all entries are null
            return count ?? 0;

        }
        public async Task<IActionResult> OnPostCheckout()
        {
            //-----------------------------Payment----------------------//
            HttpContext.Session.SetString("PaymentType", "Product");
            PaymentInformationModel model = new PaymentInformationModel();
            model.OrderType = "Pay Order Product";
            model.Amount = double.Parse(HttpContext.Session.GetString("Total"));
            model.OrderDescription = "Pay Order Product";
            model.Name = HttpContext.Session.GetString("username");

            string url = _vnPayService.CreatePaymentUrl(model, HttpContext);

            return Redirect(url);
        }
    }
}
