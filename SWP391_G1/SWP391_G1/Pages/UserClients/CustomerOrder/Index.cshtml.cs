using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SWP391_G1.Models;
using SWP391_G1.Services;

namespace SWP391_G1.Pages.UserClients.CustomerOrder
{
    public class IndexModel : PageModel
    {
        public static SWP391_G1.Models.SakuraCosmeticContext context = new Models.SakuraCosmeticContext();

        PaginatedList<Order> orders = new PaginatedList<Order>(context.Orders.ToList(), context.Orders.Count(), 1, 6);
        PaginatedList<Reservation> resevations = new PaginatedList<Reservation>(context.Reservations.ToList(), context.Orders.Count(), 1, 6);

        [BindProperty]
        public User User { get; set; } = default!;

        [BindProperty]
        public List<Order> Orders { get; set; } = default!;

        [BindProperty]
        public List<Reservation> Reservation { get; set; } = default!;

        [BindProperty(SupportsGet = true)]
        public int IndexPaging { get; set; } = 1;

        [BindProperty(SupportsGet = true)]
        public int IndexPagingService { get; set; } = 1;

        public int TotalPage { get; set; }
        public int TotalPageService { get; set; }
        public IActionResult OnGet()
        {
            var user = context.Users.FirstOrDefault(u => u.UserId == HttpContext.Session.GetInt32("id"));
            //var order = context.Orders.Where(o=>o.UserId == HttpContext.Session.GetInt32("id")).ToList();
            //var reservation = context.Reservations.Where(r => r.UserId == HttpContext.Session.GetInt32("id")).ToList();

            
            if (user == null)
            {
                return RedirectToPage("/Index");
            }

            var listOrder = context.Orders.Where(o => o.UserId == HttpContext.Session.GetInt32("id")).ToList();
            orders = new PaginatedList<Order>(listOrder, listOrder.Count, IndexPaging, 5);

            TotalPage = orders.TotalPages;
            Orders = PaginatedList<Order>.Create(orders.AsQueryable<Order>(), IndexPaging, 5);

            //resevation
            var listService = context.Reservations.Where(o => o.UserId == HttpContext.Session.GetInt32("id")).ToList();
            resevations = new PaginatedList<Reservation>(listService, listService.Count, IndexPagingService, 5);

            TotalPageService = resevations.TotalPages;
            Reservation = PaginatedList<Reservation>.Create(resevations.AsQueryable<Reservation>(), IndexPagingService, 5);


            User = user;
            //Reservation = reservation;
            return Page();
        }
    }
}
