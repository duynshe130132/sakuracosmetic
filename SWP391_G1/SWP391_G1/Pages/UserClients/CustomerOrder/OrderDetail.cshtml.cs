using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using SWP391_G1.Models;

namespace SWP391_G1.Pages.UserClients.CustomerOrder
{
    
    public class OrderDetailModel : PageModel
    {
        private readonly SWP391_G1.Models.SakuraCosmeticContext context = new Models.SakuraCosmeticContext();
        [BindProperty]
        public List<OrderDetail> OrderDetail { get; set; } = default!;

        [BindProperty]
        public List<ReservationDetail> ReservationDetail { get; set; } = default!;

        public IActionResult OnGet(int? id, int? rid)
        {
            var orderdetail = context.OrderDetails.Where(o=>o.OrderId == id).Include("Product").Include("Order").ToList();

            var resevationdetail = context.ReservationDetails.Where(o => o.ReservationId == rid).Include("Reservation").Include("Service").ToList();

            OrderDetail = orderdetail;
            ReservationDetail = resevationdetail;

            return Page();
        }
    }
}
