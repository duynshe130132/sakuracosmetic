using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SWP391_G1.Models;
using SWP391_G1.Services;

namespace SWP391_G1.Pages.UserClients.Blogs
{
    public class IndexModel : PageModel
    {
      
            public static SakuraCosmeticContext context = new SakuraCosmeticContext();
        PaginatedList<Blog> blogs = new PaginatedList<Blog>(context.Blogs.Include(b => b.Cate).ToList(), context.Blogs.Count(), 1, 6);
        [BindProperty(SupportsGet = true)]
        public string? SearchString { get; set; } 

        public SelectList? Categories { get; set; }

        [BindProperty(SupportsGet = true)]
        public int CategoryId { get; set; } = 0;

        [BindProperty(SupportsGet = true)]
        public int IndexPaging { get; set; } = 1;

        [BindProperty(Name = "id", SupportsGet = true)]
        public int Id { get; set; }
                                                                                                    
        public int TotalPage { get; set; }
        [BindProperty(SupportsGet = true)]
        public Blog Blog { get; set; }
        public int BlogCount { get; set; }
        [BindProperty(SupportsGet = true)]
        public IList<BlogCategory> BlogCategory { get; set; } = default;
        [BindProperty(SupportsGet = true)]
        public IList<Blog> Blogs { get; set; } = default;
        [BindProperty(SupportsGet = true)]
        public int PageSize { get; set; } = default;
        //[BindProperty(SupportsGet = true)]
        //public ReservationCart cart { get; set; } = default;
        public void OnGet(/*int categoryId, string searchString, int indexPaging*/)
        {

            //PaginatedList<Service> services;
            if (CategoryId != 0)
            {
                var listBlog = String.IsNullOrEmpty(SearchString) ? context.Blogs.Include(b => b.Cate).Where(b => b.CateId == CategoryId).ToList() : context.Blogs.Include(b => b.Cate).Where(b => b.CateId == CategoryId && b.BlogTitle.Contains(SearchString)).ToList();
                blogs = new PaginatedList<Blog>(listBlog, listBlog.Count, 1, 6);
            }
            else
            {
                var listBlog = String.IsNullOrEmpty(SearchString) ? context.Blogs.Include(b => b.Cate).ToList() : context.Blogs.Include(b => b.Cate).Where(b => b.BlogTitle.Contains(SearchString)).ToList();
                blogs = new PaginatedList<Blog>(listBlog, listBlog.Count, 1, 6);
            }

            TotalPage = blogs.TotalPages;
            List<BlogCategory> blogCategory = context.BlogCategories.ToList();
            BlogCount = context.Blogs.ToList().Count;
            BlogCategory = blogCategory;
            ViewData["categoryList"] = blogCategory;
            ViewData["CategoryId"] = new SelectList(blogCategory, "CategoryId", "CategoryName");
            Blogs = PaginatedList<Blog>.Create(blogs.AsQueryable<Blog>(), IndexPaging, 6);
            ViewData["StaffId"] = new SelectList(context.Employees, "Id", "FullName");
        }
     
    }
    }

