﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using SWP391_G1.Models;
using SWP391_G1.Services;

namespace SWP391_G1.Pages.UserClients.Blogs
{
    public class BlogDetailModel : PageModel
    {
        public static SWP391_G1.Models.SakuraCosmeticContext context = new Models.SakuraCosmeticContext();

        PaginatedList<Comment> comments = new PaginatedList<Comment>(context.Comments.ToList(), context.Comments.Count(), 1, 6);

        [BindProperty(SupportsGet = true)]
        public int IndexPaging { get; set; } = 1;


        [BindProperty(SupportsGet = true)]
        public Comment comment { get; set; } = default;


        [BindProperty(SupportsGet = true)]
        public IList<Comment> Comments { get; set; } = default;


        [BindProperty(SupportsGet = true)]
        public Blog Blog { get; set; } = default!;


        [BindProperty]
        public string commentContext { get; set; }


        [BindProperty(SupportsGet = true)]
        public int BlogId { get; set; }

        public int TotalPage { get; set; }
        public void OnGet(int? id)
        {
            Blog = context.Blogs.FirstOrDefault(b => b.BlogId == id);

            var listComment = context.Comments.Where(c => c.BlogId == id).OrderByDescending(c=>c.Id).ToList();

            comments = new PaginatedList<Comment>(listComment, listComment.Count(), IndexPaging, 2);

            TotalPage = comments.TotalPages;

            Comments = PaginatedList<Comment>.Create(comments.AsQueryable<Comment>(), IndexPaging, 2);

            BlogId = id.Value;
        }

        public IActionResult OnPostComment()
        {
            comment = new Comment()
            {
                Comment1 = commentContext,
                UserId = HttpContext.Session.GetInt32("id"),
                BlogId = BlogId
            };
            context.Add(comment);
            context.SaveChanges();
            return RedirectToPage("/UserClients/Blogs/BlogDetail", new { id = BlogId });
        }
    }
}
