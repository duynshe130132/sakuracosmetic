using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SWP391_G1.Models;
using SWP391_G1.Services;
using SWP391_G1.VnPaymentService;
using System.Net.Http;
using System.Text.Json;
using static Google.Apis.Requests.BatchRequest;

namespace SWP391_G1.Pages.UserClients.Payment
{
    public class PaymentConfirmModel : PageModel
    {
        private readonly SakuraCosmeticContext context;
        private readonly PaymentService _vnPayService = null;
        public PaymentConfirmModel(PaymentService vnPayService, SakuraCosmeticContext _context)
        {
            _vnPayService = vnPayService;
            context = _context;
        }
        [BindProperty(SupportsGet = true)]
        public PaymentResponseModel paymentResponse { get; set; }
        public IActionResult OnGet()
        {

            paymentResponse = _vnPayService.PaymentExecute(Request.Query);
            if (paymentResponse.TransactionId.Equals("0"))
            {
                return RedirectToPage("/Index");
            }
            HttpContext.Session.SetInt32("id", Int32.Parse(HttpContext.Items["id"].ToString()));
            HttpContext.Session.SetString("password", HttpContext.Items["password"].ToString());
            HttpContext.Session.SetString("username", HttpContext.Items["username"].ToString());
            HttpContext.Session.SetString("avatar", HttpContext.Items["avatar"].ToString());
            string[] parts = paymentResponse.OrderDescription.Split(' ');
            if (paymentResponse.OrderDescription.Contains("Product"))
            {
                
                Order order = new Order();
                order.OrderDate = DateTime.Now;
                order.UserName = parts[0];
                order.Total = decimal.Parse(parts[4]);
                order.UserId = HttpContext.Session.GetInt32("id");
                order.Status = paymentResponse.Success;
                context.Orders.Add(order);
                context.SaveChanges();
            }
            if (paymentResponse.OrderDescription.Contains("Service"))
            {

                Reservation reservation = new Reservation();
                reservation.CreatedDate = DateTime.Now;
                reservation.UserId = HttpContext.Session.GetInt32("id");
                reservation.Status = paymentResponse.Success;
                context.Reservations.Add(reservation);
                context.SaveChanges();
            }
            return Page();
        }
        public IActionResult OnPostPaymentCheck()
        {
            var type = HttpContext.Session.GetString("PaymentType");
            if (type.Contains("Product"))
            {
                var username = HttpContext.Session.GetString("username");
                Order order = context.Orders.Where(x => x.UserName.Equals(username)).OrderByDescending(x => x.OrderDate).First();
                List<CartDTO> cartDTOs = JsonSerializer.Deserialize<List<CartDTO>>(HttpContext.Session.GetString("ProductCarts"));
                foreach (var item in cartDTOs)
                {
                    OrderDetail orderDetail = new OrderDetail();
                    orderDetail.OrderId = order.OrderId;
                    orderDetail.ProductId = item.ProductId;
                    orderDetail.Quantity = item.Count;
                    Product p = context.Products.FirstOrDefault(x => x.ProductId == item.ProductId);
                    orderDetail.UnitPrice = p.ProductPrice * item.Count;
                    context.OrderDetails.Add(orderDetail);
                }
                context.SaveChanges();
            }
            if (type.Contains("Service"))
            {
                var userid = HttpContext.Session.GetInt32("id");
                Reservation reservation = context.Reservations.Where(x=>x.UserId==userid).OrderByDescending(x => x.CreatedDate).First();
                List<ReservationCartDTO> cartDTOs = JsonSerializer.Deserialize<List<ReservationCartDTO>>(HttpContext.Session.GetString("ReservationCarts"));
                foreach(var item in cartDTOs)
                {
                    ReservationDetail reservationDetail = new ReservationDetail();
                    reservationDetail.ServiceId = item.ServiceId;
                    reservationDetail.StaffId = item.StaffId;
                    Service service = context.Services.FirstOrDefault(x => x.ServiceId == item.ServiceId);
                    reservationDetail.ServicePrice = service.Price;
                    reservationDetail.ReserTime = item.ReservationTime;
                    reservationDetail.ReservationId = reservation.ReservationId;
                    context.ReservationDetails.Add(reservationDetail);
                }
                context.SaveChanges();
            }
                return RedirectToPage("/UserClients/CustomerOrder/Index");
        }
    }
}
