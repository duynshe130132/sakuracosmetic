using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SWP391_G1.Models;
using SWP391_G1.Services;

namespace SWP391_G1.Pages.UserClients.Profile
{
    public class IndexModel : PageModel
    {
        private readonly SWP391_G1.Models.SakuraCosmeticContext context = new SakuraCosmeticContext();
        private readonly FlieUploadService uploadservice = new FlieUploadService();


        [BindProperty]
        public User User { get; set; } = default!;
        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (HttpContext.Session.GetInt32("id") == null)
            {
                return RedirectToPage("/Index");
            }
            var user = context.Users.SingleOrDefault(e => e.UserId == HttpContext.Session.GetInt32("id"));
            if (user == null)
            {
                return NotFound();
            }
            User = user;
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(IFormFile file)
        {
            var user = context.Users.SingleOrDefault(e => e.UserId == HttpContext.Session.GetInt32("id"));


            if (!ModelState.IsValid)
            {
                return Page();
            }
            var filePath = await uploadservice.DriveUploadBasic(file);
            user.AvatarUrl = filePath;

            user.FullName = User.FullName;
            user.Address = User.Address;
            user.City = User.City;
            user.State = User.State;
            user.Country = User.Country;
            user.Phone = User.Phone;
            user.Email = User.Email;
            //context.Attach(Employee).State = EntityState.Modified;

            context.SaveChanges();

            HttpContext.Session.SetString("UpdateSuccess", "Update Success !!! ");
            return Page();


        }
    }
}
