﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SWP391_G1.Models;
using System.Reflection.Metadata;

namespace SWP391_G1.Pages
{
    public class IndexModel : PageModel
    {
        private SakuraCosmeticContext _context = new SakuraCosmeticContext();
        private readonly ILogger<IndexModel> _logger;

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }
        [BindProperty(SupportsGet =true)]
        public List<Blog> blogs { get; set; }
        [BindProperty(SupportsGet = true)]
        public List<Service> services { get; set; }
        [BindProperty(SupportsGet = true)]
        public List<Product> products { get; set; }
        public void OnGet()
        {
            blogs = _context.Blogs.Where(x=> x.FamousStatus == true).ToList();
            services = _context.Services.Take(3).ToList();
            products = _context.Products.Take(4).ToList();


        }
       

    }
}