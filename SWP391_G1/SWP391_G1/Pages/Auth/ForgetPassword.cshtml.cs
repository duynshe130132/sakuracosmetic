﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SWP391_G1.Models;
using System.Net.Mail;
using System.Net;
using System.Net.Mail;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using SWP391_G1.Services;
using System.Text;
using BCrypt.Net;

namespace SWP391_G1.Pages.Auth
{
    public class ForgetPasswordModel : PageModel
    {

        public SakuraCosmeticContext _context = new SakuraCosmeticContext();

        [BindProperty]
        public string InputEmail { get; set; }
        public string MsgErr { get; set; }
        
        public void OnGet()
        {
        }

        public IActionResult OnPost()
        {
            var user = _context.Users.SingleOrDefault(u => u.Email == InputEmail);

            if (user != null)
            {
                // Generate a new password for the user
                //var newPassword = GenerateNewPassword();
                var code = GenerateNewPassword();

                //string hashPassword = BCrypt.Net.BCrypt.HashPassword(newPassword);
                // Save the new password to the database
                //user.Password = hashPassword;
                //_context.SaveChanges();
                HttpContext.Session.SetString("code", code);
                HttpContext.Session.SetString("mail", user.Email);
                // Send the new password to the user's email address
                SendPasswordEmail(user.Email, code);

                return RedirectToPage("/Auth/EnterCode");
            }
            MsgErr = "Email not valid!!";
            return Page();
        }

        private string GenerateNewPassword()
        {
            const string allowedChars = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+-=";
            const int passwordLength = 4;

            var random = new Random();
            var newPassword = new StringBuilder();

            for (int i = 0; i < passwordLength; i++)
            {
                int index = random.Next(allowedChars.Length);
                newPassword.Append(allowedChars[index]);
            }

            return newPassword.ToString();
        }
        private void SendPasswordEmail(string email, string newPassword)
        {
            var smtpClient = new SmtpClient("smtp.gmail.com")
            {
                Port = 587,
                Credentials = new NetworkCredential("managementcoffeeG1@gmail.com", "xdswzxbizecjpskr"),
                //chatgpt.1202@gmail.com
                EnableSsl = true, // để thiết lập một kết nối bảo mật.
                UseDefaultCredentials = false //để đảm bảo rằng không sử dụng thông tin đăng nhập mặc định của hệ thống.
            };

            var message = new MailMessage(
                "managementcoffeeG1@gmail.com",
                email,
                "Reset Password Management Sakura Cosmetic",
                $"{newPassword} là mật khẩu mới được cung cấp dành riêng cho bạn từ hệ thống Management Sakura Cosmetic. Hãy sử dụng mật khẩu này để đăng nhập và thay đổi mật khẩu của bạn!");

            smtpClient.Send(message); //phương thức Send được gọi trên đối tượng SmtpClient với đối tượng MailMessage làm tham số, để gửi email đi.
        }
    }
}

