﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SWP391_G1.Models;
using BCrypt.Net;

namespace SWP391_G1.Pages.Auth
{
    public class RegisterModel : PageModel
    {
        SakuraCosmeticContext context = new SakuraCosmeticContext();
        [BindProperty]
        public string ErrorMessagePhomeNumber { get; set; }
        [BindProperty]
        public string ErrorComfirm { get; set; }
        [BindProperty]
        public string ErrorUser { get; set; }

        [BindProperty]
        public string ErrorLength { get; set; }
        [BindProperty]
        public string name { get; set; }

        [BindProperty]
        public string username { get; set; }

        [BindProperty]
        public string password { get; set; }
        [BindProperty]
        public string confirm { get; set; }
        [BindProperty]
        public string phoneNumber { get; set; }

        [BindProperty]
        public string email { get; set; }
        [BindProperty]
        public string address { get; set; }

        [BindProperty]
        public string city { get; set; }
        [BindProperty]
        public string state { get; set; }

        [BindProperty]
        public string country { get; set; }

        [BindProperty]
        public User User { get; set; } = default!;
        public async Task<IActionResult> OnGetAsync()
        {
            
            return Page();
        }

        public IActionResult OnPost()
        {
            var exist = context.Users.FirstOrDefault(u => u.UserName == username);
           
            if (phoneNumber.Length < 10 && confirm != password && exist != null)
            {
                ErrorMessagePhomeNumber = "Số điện thoại phải có 10 chữ số";
                ErrorComfirm = "Mật khẩu không khớp";
                ErrorUser = "Tài Khoản Đã Tồn Tại";
                return Page();
            }
           if(phoneNumber.Length < 10 && confirm != password ){
                ErrorMessagePhomeNumber = "Số điện thoại phải có 10 chữ số";
                ErrorComfirm = "Mật khẩu không khớp";
            }
           if(phoneNumber.Length < 10 && exist != null)
            {
                ErrorMessagePhomeNumber = "Số điện thoại phải có 10 chữ số";
                ErrorUser = "Tài Khoản Đã Tồn Tại";
            }
           if( confirm != password && exist != null)
            {
                ErrorComfirm = "Mật khẩu không khớp";
                ErrorUser = "Tài Khoản Đã Tồn Tại";
            }

            if (phoneNumber.Length < 10 )
            {
                ErrorMessagePhomeNumber = "Số điện thoại phải có 10 chữ số";              
                return Page();
            }

            if (confirm != password)
            {
                ErrorComfirm = "Mật khẩu không khớp";
                return Page();
            }
           
            if (exist != null)
            {
                ErrorUser = "Tài Khoản Đã Tồn Tại";
                return Page();
            }
            string hashPassword = BCrypt.Net.BCrypt.HashPassword(password);
            User = new User()
            {
                FullName = name,
                UserName = username,
                Password = hashPassword,
                Address = address,
                City = city,
                State = state,
                Country = country,
                Phone= phoneNumber,
                Email = email,
                AvatarUrl = "http.com"
                       };
            context.Add(User);

            context.SaveChanges();
            HttpContext.Session.SetString("NotificationSuccess", "Bạn đã đăng ký thành công");
            return Page();
        }
    }
}

