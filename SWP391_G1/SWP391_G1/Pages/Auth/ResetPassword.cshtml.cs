﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SWP391_G1.Models;
using System.ComponentModel.DataAnnotations;

namespace SWP391_G1.Pages.Auth
{
    public class ResetPasswordModel : PageModel
    {
        SakuraCosmeticContext context = new SakuraCosmeticContext();
        [BindProperty]      
        public string Error_Confirm { get; set; }
       
        [BindProperty]
        public string new_password { get; set; }
        [BindProperty]
        public string confirm { get; set; }
        public IActionResult OnGet()
        {
            if (HttpContext.Session.GetString("code") == null)
            {
                return RedirectToPage("/Index");
            }
            return Page();
        }
        public IActionResult OnPost()
        {
            
            if (new_password != confirm)
            {
                Error_Confirm = "Không khớp với Mật Khẩu mới";
                return Page();
            }
            string hashPassword = BCrypt.Net.BCrypt.HashPassword(new_password);

            var user = context.Users.SingleOrDefault(u => u.Email == HttpContext.Session.GetString("mail"));
            user.Password = hashPassword;

            context.SaveChanges();

            HttpContext.Session.SetString("NotificationSuccess", "Bạn đã đổi mật khẩu thành công");    
            return Page();
        }
    }
}
