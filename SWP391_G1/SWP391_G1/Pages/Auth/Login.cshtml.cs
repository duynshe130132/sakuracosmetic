﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using SWP391_G1.Models;
using System.Security.Cryptography.X509Certificates;
using BCrypt.Net;

namespace SWP391_G1.Pages.Auth
{
    public class LoginModel : PageModel
    {
        SakuraCosmeticContext context = new SakuraCosmeticContext();

        [BindProperty]
        public string errorPassword { get; set; }
        [BindProperty]
        public string username { get; set; }

        [BindProperty]
        public string password { get; set; }

        public IActionResult OnGet()
        {
            if (HttpContext.Session.GetString("username") != null && HttpContext.Session.GetInt32("RoleId") != null)
            {
                return RedirectToPage("/Admin/Services/Index");
            }
            else if (HttpContext.Session.GetString("username") != null)
            {
                return RedirectToPage("/Index");
            }

            return Page();
        }

        public IActionResult OnPost()
        {
            HttpContext.Session.Clear();
            var user = context.Users.FirstOrDefault(u => u.UserName.Equals(username));


            if (user != null && BCrypt.Net.BCrypt.Verify(password, user.Password))
            {
                HttpContext.Session.SetString("username", username);
                HttpContext.Session.SetString("avatar", user.AvatarUrl);
                HttpContext.Session.SetInt32("id", user.UserId);
                HttpContext.Session.SetString("password", user.Password);
                return RedirectToPage("/Index");
            }

            var employee = context.Employees.FirstOrDefault(u => u.UserName == username && u.RoleId == 1);
            if (employee != null && BCrypt.Net.BCrypt.Verify(password, employee.Password))
            {
                HttpContext.Session.SetString("username", username);
                HttpContext.Session.SetInt32("RoleId", employee.RoleId ?? 0);
                HttpContext.Session.SetInt32("id", employee.Id);
                HttpContext.Session.SetString("avatar", employee.AvatarUrl);

                return RedirectToPage("/Admin/Services/Index");
            }
            errorPassword = "Sai Tài Khoản/Mật Khẩu ! Vui Lòng Nhập Lại";
            return Page();

        }


    }
}
