﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SWP391_G1.Models;
using BCrypt.Net;
namespace SWP391_G1.Pages.Auth
{
    public class ChangePasswordModel : PageModel
    {
        SakuraCosmeticContext context = new SakuraCosmeticContext();
         [BindProperty]
        public string ErrorPassword { get; set; }
        [BindProperty]
        public string Error_Confirm{ get; set; }

        [BindProperty]
        public string password { get; set; }
        [BindProperty]
        public string new_password { get; set; }
        [BindProperty]
        public string confirm { get; set; }
        public IActionResult OnGet()
        {
            if(HttpContext.Session.GetString("id") == null)
            {
                return RedirectToPage("/Index");
            }
            return Page();
        }
        public IActionResult OnPost()
        {
            if(BCrypt.Net.BCrypt.Verify(password, HttpContext.Session.GetString("password") ) == false)
            {
                ErrorPassword = "Mat khau khong dung voi mat khau cu";
                return Page();
            }
            if(new_password != confirm)
            {
                Error_Confirm = "Không khớp với Mật Khẩu mới";
                return Page();
            }
            string hashPassword = BCrypt.Net.BCrypt.HashPassword(new_password);

            var user = context.Users.SingleOrDefault(u=>u.UserId == HttpContext.Session.GetInt32("id"));
            user.Password = hashPassword;

            context.SaveChanges();

            HttpContext.Session.SetString("NotificationSuccess", "Bạn đã đổi mật khẩu thành công");

            return Page();
        }
        
    }
}
