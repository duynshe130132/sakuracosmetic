﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SWP391_G1.Models;
using System.ComponentModel.DataAnnotations;

namespace SWP391_G1.Pages.Auth
{
    public class EnterCodeModel : PageModel
    {


        [BindProperty]
        [Required(ErrorMessage = "Vui lòng nhập mã xác nhận.")]
        public string Code { get; set; }

        public IActionResult OnGet()
        {
            if (HttpContext.Session.GetString("code") == null)
            {
                return RedirectToPage("/Index");
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (Code == HttpContext.Session.GetString("code"))
            {
                return RedirectToPage("/Auth/ResetPassword");
            }
            ViewData["Message"] = "Mã xácnhận không chính xác. Vui lòng thử lại.";
            return Page();
        }
    }
}
