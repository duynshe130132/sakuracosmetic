﻿namespace SWP391_G1.Services
{
    public class ReservationCartDTO
    {
        public int RecordId { get; set; }
        public int? ServiceId { get; set; }
        public int? StaffId { get; set; }
        public DateTime ReservationTime { get; set; }
        public DateTime? DateCreated { get; set; }
    }
}
