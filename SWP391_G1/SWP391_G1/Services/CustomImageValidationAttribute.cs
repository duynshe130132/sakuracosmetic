﻿using System.ComponentModel.DataAnnotations;

namespace SWP391_G1.Services
{
    public class CustomImageValidationAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var file = value as IFormFile;

            if (file != null)
            {
                // Perform your custom validation logic here
                // For example, you can check the file size, dimensions, format, etc.
                if (file.Length > (25 * 1024 * 1024))
                {
                    return new ValidationResult("The file size exceeds the limit.");
                }
                if (!file.ContentType.StartsWith("image/"))
                {
                    return new ValidationResult("The file type is not true.");
                }
            }

            return ValidationResult.Success;
        }
    }
}
