﻿namespace SWP391_G1.Services
{
    public class CartDTO
    {
        public int RecordId { get; set; }
        public int ProductId { get; set; }
        public int Count { get; set; }
        public DateTime? DateCreated { get; set; }
    }
}
