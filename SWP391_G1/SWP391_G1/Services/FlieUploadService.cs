﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Upload;
using Google.Apis.Util.Store;
using System;
using System.Security.AccessControl;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
namespace SWP391_G1.Services
{
    public class FlieUploadService
    {


        private const string PathToServiceAccountKey = "sakuracosmetic.json";
        private const string ServiceAccountEmail = "swp391@sakuracosmetic.iam.gserviceaccount.com";
        private const string DirectoryId = "1NeDNCRqmInt9ptqDrb4ks_bG3XJy9RM7";
        public async Task<string> DriveUploadBasic(IFormFile file)
        {
            long fileSizeInBytes = file.Length;
            long MaxFileSizeInBytes = 25 * 1024 * 1024;
            if (fileSizeInBytes > MaxFileSizeInBytes)
            {
                // Xử lý khi file vượt quá kích thước tối đa cho phép
                // ...
                return "Overload"; 
            }
            var credential = GoogleCredential.FromFile(PathToServiceAccountKey).CreateScoped(DriveService.ScopeConstants.Drive);
            var service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential
            });
            var fileMetadata = new Google.Apis.Drive.v3.Data.File()
            {
                Name = file.FileName,
                Parents = new List<string>() { DirectoryId }
            };
            string uploadId;

            var filePath = Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot\image", file.FileName);
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
            }
            using (var fsSource = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                var request = service.Files.Create(fileMetadata, fsSource, "image/jpeg");
                request.Fields = "*";
                var results = await request.UploadAsync(CancellationToken.None);
                if(results.Status == UploadStatus.Failed)
                {
                    return null;
                }
                uploadId = request.ResponseBody?.Id;
            }
            //File.Delete(filePath);
            return uploadId;
        }
        public async Task<bool> DeleteFile(string id)
        {
            var credential = GoogleCredential.FromFile(PathToServiceAccountKey).CreateScoped(DriveService.ScopeConstants.Drive);
            var service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential
            });
            try
            {
                await service.Files.Delete(id).ExecuteAsync();
                return true;
            }
            catch (Exception ex)
            {
                // Xử lý lỗi nếu cần thiết
                Console.WriteLine("Lỗi xóa tệp tin: " + ex.Message);
                return false;
            }
        }
    }
}
